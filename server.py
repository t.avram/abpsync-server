import json
import os
import uuid

from flask import Flask, request, jsonify, Response, render_template
from flask_cors import CORS, cross_origin
from flask_mail import Mail, Message


DATA_ROOT = 'data/'

ids = []

app = Flask(__name__)
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'sync.filters@gmail.com'
app.config['MAIL_PASSWORD'] = 'ABPfilters'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

CORS(app)
mail = Mail()
mail.init_app(app)


def _full_path(file_name):
    return os.path.join(DATA_ROOT, file_name)


def _generate_id():
    newID = uuid.uuid4().hex

    while newID in ids:
        newID = uuid.uuid4().hex

    ids.append(newID)

    return newID


def _load_ids():
    if not os.path.isdir(DATA_ROOT):
        pass

    global ids
    ids = [os.path.splitext(file)[0]
           for file in os.listdir(DATA_ROOT)
           if os.path.isfile(os.path.join(DATA_ROOT, file))
           and file.endswith('.json')]


def _err_response(code, err):
    return Response(status=code, response=str(err))


@app.route('/upload', methods=['POST'])
@cross_origin()
def upload():
    try:
        content = request.get_json()
        user_id = content.pop('id')

        file_name = '{}.json'.format(user_id)

        with open(_full_path(file_name), 'w') as out_stream:
            json.dump(content, out_stream)
        return jsonify({'success': True})
    except KeyError as err:
        return _err_response(400, err)
    except Exception as err:
        return _err_response(500, err)


@app.route('/id', methods=['GET'])
@cross_origin()
def get_id():
    try:
        newID = _generate_id()
        return jsonify({'success': True, 'id': newID})
    except Exception as err:
        return _err_response(500, err)


@app.route('/download', methods=['POST'])
@cross_origin()
def get_data():
    try:
        user_id = request.form['id']

        file_path = os.path.join(DATA_ROOT, '{}.json'.format(user_id))
        if not os.path.isfile(file_path):
            return _err_response(400, 'Cannot find ID in database')

        with open(file_path) as stream:
            raw_data = json.load(stream)

        return jsonify(raw_data)

    except KeyError as err:
        return _err_response(400, err)
    except Exception as err:
        return _err_response(500, err)


@app.route('/email', methods=['POST'])
@cross_origin()
def send_email():
    try:
        mail.connect()
        user_id = request.form['id']
        email = request.form['email']
        html_contents = render_template('email_template.html', id=user_id)
        message = Message("Your ABP id",
                          sender=('ABP team', 'a96tudor@gmail.com'))
        message.html = html_contents
        message.recipients = [email]
        mail.send(message)
        return Response()
    except KeyError as err:
        return _err_response(400, err)
    except Exception as err:
        return _err_response(500, err)


if not os.path.isdir(DATA_ROOT):
    os.makedirs(DATA_ROOT)

_load_ids()
app.run(debug=True)
