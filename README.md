## API specs

1. Get all settings

|     Parameter  |    Value  |
|:--------------:|:--------:|
|    path    | `/download` |
|    method    |    POST   |
|       Content      |    ```{"id": <the user id>}```

It will return a JSON with the following format:

```json
{
  "filterLists": [
      {
        "url": <the url of the filterlist>,
        "title": <the title of the filterlist>
      },
      ...
  ],
  "customFilters": [
    {
      "filter": <the actual filter>,
      "disabled": <whether the filter is diasbled or not>
    },
    ....
  ]
  "whitelistedWebsites": [
    {
      "website": <url of the whitelisted website>,
    }
    ...
  ]
}
```
                                                                                                                                                                                                                                                             